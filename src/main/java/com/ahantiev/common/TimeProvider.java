package com.ahantiev.common;

import java.time.Clock;
import java.time.LocalDateTime;
import java.time.ZoneOffset;

public class TimeProvider {
    private static Clock clock = Clock.systemUTC();

    public static LocalDateTime now() {
        return LocalDateTime.now(clock);
    }

    public static void useFixedClockAt(LocalDateTime time) {
        clock = Clock.fixed(time.toInstant(ZoneOffset.UTC), ZoneOffset.UTC);
    }
}