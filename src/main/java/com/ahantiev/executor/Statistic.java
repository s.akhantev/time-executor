package com.ahantiev.executor;

import java.util.concurrent.atomic.AtomicLong;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

public class Statistic {
    private static final Logger log = LogManager.getLogger(Statistic.class);

    final AtomicLong allCount = new AtomicLong(0);
    final AtomicLong canceledCount = new AtomicLong(0);
    final AtomicLong successfulCount = new AtomicLong(0);
    final AtomicLong failedCount = new AtomicLong(0);

    public long getCanceledCount() {
        return canceledCount.get();
    }

    public long getSuccessfulCount() {
        return successfulCount.get();
    }

    public long getFailedCount() {
        return failedCount.get();
    }

    public void print() {
        log.info("Statistic: all: {}, successfulCount: {}, failedCount: {}, cancelled {}",
                allCount.get(),
                successfulCount.get(),
                failedCount.get(),
                canceledCount.get());
    }
}
