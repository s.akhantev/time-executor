package com.ahantiev.executor;

import java.time.LocalDateTime;
import java.util.List;
import java.util.Objects;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.Callable;
import java.util.concurrent.DelayQueue;
import java.util.concurrent.PriorityBlockingQueue;
import java.util.concurrent.atomic.AtomicLong;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

public class Executor extends Thread {
    private static final Logger log = LogManager.getLogger(Executor.class);

    private final Statistic statistic = new Statistic();
    private final AtomicLong sequencer = new AtomicLong();
    private final BlockingQueue<Task> waitingQueue = new DelayQueue<>();
    private final BlockingQueue<Task> backlogQueue = new PriorityBlockingQueue<>();
    private final List<Worker> workers;

    private boolean running = false;

    public Executor(int workersCount) {
        if (workersCount < 1) {
            throw new IllegalArgumentException("workersCount must be > 0");
        }
        this.workers = IntStream.range(0, workersCount)
                .mapToObj(id -> new Worker("Worker-" + id, backlogQueue, statistic))
                .collect(Collectors.toList());
    }

    public synchronized void start() {
        if (running) {
            throw new IllegalStateException("Executor already started");
        }
        running = true;
        log.info("Start executor with {} worker(s)", workers.size());
        this.workers.forEach(Worker::start);
        super.start();
    }

    public <V> Task<V> submit(Callable<V> callable, LocalDateTime startTime) {
        Objects.requireNonNull(callable);
        Objects.requireNonNull(startTime);

        TaskImpl<V> task = new TaskImpl<>(callable, startTime, sequencer.incrementAndGet());
        waitingQueue.add(task);
        statistic.allCount.incrementAndGet();
        return task;
    }

    @Override
    public void run() {
        while (isRunning()) {
            try {
                Task next = waitingQueue.take();
                backlogQueue.add(next);
            } catch (InterruptedException ignored) {
                log.trace("Executor interrupted");
                shutdown();
            }
        }
        log.info("Executor stopped");
    }

    public Statistic getStatistic() {
        return statistic;
    }

    public void shutdown() {
        log.info("Executor stopping...");
        running = false;
        backlogQueue.forEach(task -> task.cancel(true));
    }

    public boolean isRunning() {
        return running && !this.isInterrupted();
    }
}
